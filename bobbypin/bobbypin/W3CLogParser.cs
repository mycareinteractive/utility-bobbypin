﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace bobbypin
{
    public class W3CLogParser
    {
        private Dictionary<string, string> ipAddresses = new Dictionary<string, string>();

        /// <summary>
        /// RegEx expression to search for
        /// </summary>
        public string Pattern { get; set; }

        /// <summary>
        /// Only parse the latest X seconds.  By default it is -1 meaning parse everything.
        /// Eg: Set this to 5 will cause Parser only parse the latest 5 minutes log.
        /// </summary>
        public int ParseSpanInMinutes { get; set; }

        public Dictionary<string, string> Result
        {
            get
            {
                return ipAddresses;
            }
        }

        public W3CLogParser()
        {
            Pattern = @"GET /startup/";
            ParseSpanInMinutes = -1;
        }

        ~W3CLogParser()
        {

        }

        public void Reset()
        {
            if (ipAddresses != null)
            {
                ipAddresses.Clear();
            }
        }

        public void Parse(string filePath)
        {
            string line;
            DateTime since = DateTime.MinValue;
            if (ParseSpanInMinutes != -1)
            {
                since = DateTime.Now.Subtract(TimeSpan.FromMinutes(ParseSpanInMinutes));
            }

            Regex r = new Regex(Pattern, RegexOptions.IgnoreCase);

            if (!File.Exists(filePath))
            {
                Console.WriteLine("Can not find \"{0}\", ignored", filePath);
                return;
            }

            try
            {
                Stream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (var file = new StreamReader(stream))
                {
                    Console.Write("Parsing \"{0}\"......", filePath);
                    int count = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        if (r.IsMatch(line))
                        {
                            // line matches
                            var parts = line.Split(' ');
                            if(parts.Length >= 9)
                            {
                                string ip = parts[8];
                                string time = parts[0] + " " + parts[1];
                                var utc = DateTime.ParseExact(time, "yyyy-MM-dd HH:mm:ss", null, System.Globalization.DateTimeStyles.AssumeUniversal);
                                var d = utc.ToLocalTime();
                                if (DateTime.Compare(d, since) >= 0)
                                {
                                    ipAddresses[ip] = "";
                                    count++;
                                }
                            }
                            
                        }
                    }
                    Console.WriteLine("{0} matches", count);
                }
            }
            catch
            {
                Console.WriteLine("Error reading {0}.  A common reason is file is being used.", filePath);
            }
        }
    }
}
