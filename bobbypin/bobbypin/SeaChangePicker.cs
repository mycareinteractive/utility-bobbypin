﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace bobbypin
{
    public class SeaChangePicker : BasePicker
    {
        public override void Pick(Options options)
        {
            base.Pick(options);

            var now = DateTime.Now;
            var utcNow = DateTime.UtcNow;

            if (now.Hour == 0 && now.Minute <= 10)
            {
                Console.WriteLine("WFES log file is just generated at midnight and will not have enough data to check.  This opeartion is ignored.  Please retry after 12:10AM.");
                return;
            }

            var dateString = now.ToString("yyyy-MM-dd");
            var fileNames = Directory.GetFiles(options.Path, string.Format("hit_log.{0}.??.txt", dateString));

            // parse latest 2 log files
            var lockParser = new SeaChangeLogParser();
            lockParser.Pattern = @"GET /stbservlet\?attribute=json_libs_oss_get_user_data";
            lockParser.ParseSpanInMinutes = 30;
            Console.WriteLine("\nParsing realtime log for current lockups...");
            for (int i = 0; i < 2; i++)
            {
                var idx = fileNames.Length - 1 - i;
                if (idx >= 0)
                {
                    var fileName = fileNames[idx];
                    lockParser.Parse(fileName);
                }
            }

            // These IPs has been doing get_user_data
            var userDataResult = lockParser.Result;
            string userDataIps = "''";
            foreach (var ip in userDataResult.Keys)
            {
                userDataIps += string.Format(",'{0}'", ip);
            }

            // parse each hit log file
            var reloadParser = new SeaChangeLogParser();
            Console.WriteLine("\nParsing all logs for reload/reboot...");
            foreach (var fileName in fileNames)
            {
                reloadParser.Parse(fileName);
            }

            // These IPs has reloaded/rebooted
            var reloadResult = reloadParser.Result;
            string reloadIps = "''";
            foreach (var ip in reloadResult.Keys)
            {
                reloadIps += string.Format(",'{0}'", ip);
            }

            // check database for locked up devices (which do not show up in the hit address list)
            List<Device> potentialDevices = new List<Device>();
            List<Device> lockedDevices = new List<Device>();

            string connString = string.Format("Server={0};Database=multiverse;User Id=multiverse;Password=multiverse;", options.DatabaseHost);
            using (SqlConnection connection = new SqlConnection(connString))
            {
                connection.Open();

                string selectString = string.Format("SELECT hotel_id,room_id,tv_number,device_ip,device_id FROM hm_device WHERE device_ip NOT in ({0}) ORDER BY room_id", userDataIps);
                using (SqlCommand cmd = new SqlCommand(selectString, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                var device = new Device(reader["hotel_id"] as string,
                                    reader["room_id"] as string,
                                    reader["tv_number"] as string,
                                    reader["device_id"] as string,
                                    reader["device_ip"] as string);
                                if (!device.Room.StartsWith("999"))
                                {
                                    lockedDevices.Add(device);
                                    Console.Write(device.ToString());
                                    Console.WriteLine(" - LOCKED!");
                                }
                            }
                        }
                    }
                }

                selectString = string.Format("SELECT hotel_id,room_id,tv_number,device_ip,device_id FROM hm_device WHERE device_ip NOT in ({0}) ORDER BY room_id", reloadIps);
                using (SqlCommand cmd = new SqlCommand(selectString, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                var device = new Device(reader["hotel_id"] as string,
                                    reader["room_id"] as string,
                                    reader["tv_number"] as string,
                                    reader["device_id"] as string,
                                    reader["device_ip"] as string);
                                if (!device.Room.StartsWith("999") && !lockedDevices.Contains(device))
                                {
                                    potentialDevices.Add(device);
                                    Console.WriteLine(device.ToString());
                                }
                            }
                        }
                    }
                }
            }

            string eventMsg = string.Format("{0} LOCKED devices found, {1} potential locking devices found.", lockedDevices.Count, potentialDevices.Count);
            string eventDetails = "";
            Console.WriteLine(eventMsg);

            // log to file and try reboot if needed
            string proberPath = Path.Combine(Environment.CurrentDirectory, @"prober-local\prober.exe");
            int rebooted = 0;
            if (!File.Exists(proberPath))
            {
                options.AutoReboot = false; // can't reboot if Prober not found
            }
            if (options.AutoReboot)
            {
                int rebootAttempts = 1;
                Console.WriteLine("Attempting to auto reboot these devices...");

                dateString = DateTime.Now.ToString("yyyyMMdd");
                string filePath = Path.Combine(Environment.CurrentDirectory, string.Format("output.{0}.txt", dateString));
                using (StreamWriter outputFile = File.AppendText(filePath))
                using (var memStream = new MemoryStream())
                using (var outputBuff = new StreamWriter(memStream))
                {
                    outputFile.WriteLine(now.ToString());
                    outputFile.WriteLine(eventMsg);
                    outputFile.Flush();

                    outputBuff.WriteLine(now.ToString());
                    outputBuff.WriteLine(eventMsg);
                    outputBuff.Flush();

                    foreach (var device in lockedDevices)
                    {
                        outputFile.Write(device.ToString());
                        outputBuff.Write(device.ToString());
                        if (options.AutoReboot)
                        {
                            try
                            {
                                for (int i = 0; i < rebootAttempts; i++)
                                {
                                    var p = Process.Start(proberPath, string.Format("-reboot {0}", device.Ip));
                                    p.WaitForExit(15000);
                                    if (!p.HasExited)
                                    {
                                        p.Kill();
                                    }

                                    if (p.ExitCode == 0)
                                    {
                                        rebooted++;
                                        outputFile.WriteLine(" - reboot ok");
                                        outputBuff.WriteLine(" - reboot ok");
                                        break;
                                    }

                                    if (i == rebootAttempts - 1)
                                    {
                                        outputFile.WriteLine(" - reboot failed");
                                        outputBuff.WriteLine(" - reboot failed");
                                    }
                                }
                            }
                            catch
                            {
                                outputFile.WriteLine(" - reboot failed");
                                outputBuff.WriteLine(" - reboot failed");
                            }
                        }
                        else
                        {
                            outputFile.WriteLine("");
                            outputBuff.WriteLine("");
                        }
                        outputFile.Flush();
                        outputBuff.Flush();
                    }

                    outputFile.WriteLine(Environment.NewLine);
                    outputBuff.WriteLine(Environment.NewLine);

                    outputBuff.Flush();
                    eventDetails = Encoding.Default.GetString((memStream.ToArray()));
                }
            }

            // log to event view
            if (options.AutoReboot)
            {
                var locked = lockedDevices.Count;
                eventMsg += string.Format(" {0} rebooted successfully", rebooted);
                WriteEventLog(eventMsg, eventDetails, locked == 0 ? EventLogEntryType.Information : EventLogEntryType.Warning);

                if (locked > 0)
                {
                    SendEmail("ALERT!! " + eventMsg, eventDetails);
                }
            }
        }
    }
}
