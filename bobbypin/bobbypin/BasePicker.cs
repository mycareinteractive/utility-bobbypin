﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace bobbypin
{
    public class BasePicker
    {
        private Options _options;

        public virtual void Pick(Options options)
        {
            _options = options;

            if(options.Mode == ServerMode.SeaChange)
            {
                if (string.IsNullOrEmpty(options.DatabaseHost))
                    options.DatabaseHost = "127.0.0.1";
                if (string.IsNullOrEmpty(options.DatabaseName))
                    options.DatabaseName = "multiverse";
                if (string.IsNullOrEmpty(options.DatabaseUser))
                    options.DatabaseUser = "multiverse";
                if (string.IsNullOrEmpty(options.DatabasePassword))
                    options.DatabasePassword = "multiverse";
            }
            else if (options.Mode == ServerMode.UpServer)
            {
                if (string.IsNullOrEmpty(options.DatabaseHost))
                    options.DatabaseHost = "127.0.0.1";
                if (options.DatabasePort == 0)
                    options.DatabasePort = 3306;
                if (string.IsNullOrEmpty(options.DatabaseName))
                    options.DatabaseName = "upserver";
                if (string.IsNullOrEmpty(options.DatabaseUser))
                    options.DatabaseUser = "upserver";
                if (string.IsNullOrEmpty(options.DatabasePassword))
                    options.DatabasePassword = "upserver";
            }

            DeleteOldOutputFiles();
        }

        protected void WriteEventLog(string subject, string message, EventLogEntryType type = EventLogEntryType.Information)
        {
            string sSource;
            string sLog;

            sSource = "Aceso BobbyPin";
            sLog = "Application";

            message = string.Format("{0}\n\n{1}", subject, message);

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, message, type, 0);
        }

        protected void SendEmail(string subject, string message)
        {
            if(string.IsNullOrEmpty(_options.SmtpHost) || string.IsNullOrEmpty(_options.SmtpTo))
            {
                Console.WriteLine("SMTP server or email address not provided, email disabled");
                return;
            }

            try
            {
                var smtpClient = new SmtpClient();
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Host = _options.SmtpHost;
                smtpClient.Port = _options.SmtpPort;
                smtpClient.Send(_options.SmtpFrom, _options.SmtpTo, subject, message);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
        }

        protected void DeleteOldOutputFiles()
        {
            string[] files = Directory.GetFiles(Environment.CurrentDirectory, "output.*.txt");

            foreach (string file in files)
            {
                try
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.CreationTime < DateTime.Now.AddMonths(-1))
                    {
                        fi.Delete();
                    }
                }
                catch
                {
                    Console.WriteLine(string.Format("Failed to delete file {0}", file));
                }
            }
        }

    }
}
