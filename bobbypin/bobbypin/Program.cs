﻿using NDesk.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;


namespace bobbypin
{
    public enum ServerMode
    {
        SeaChange = 0,
        UpServer
    }

    public class Options
    {
        public ServerMode Mode { get; set; }
        public string Path { get; set; }
        public string DatabaseHost { get; set; }
        public int DatabasePort { get; set; }
        public string DatabaseName { get; set; }
        public string DatabaseUser { get; set; }
        public string DatabasePassword { get; set; }
        public bool AutoReboot { get; set; }
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpTo { get; set; }
        public string SmtpFrom { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Head();
            bool show_help = false;

            var options = new Options
            {
                Mode = ServerMode.SeaChange,
                Path = @"C:\TVBSTrace\wfes\logs\",
                DatabaseHost = @"127.0.0.1",
                AutoReboot = false,
                SmtpPort = 25,
                SmtpFrom = "bobbypin-notification@aceso.com"
            };

            OptionSet p = new OptionSet()
                .Add("z|upserver", "support Aceso UpServer.  By default bobbypin works in legacy SeaChange mode.  By specifying this option bobbypin will work in UpServer mode", delegate (string v) { if (v != null) options.Mode = ServerMode.UpServer; })
                .Add("s|server=", "database server, default is 127.0.0.1.", delegate (string v) { options.DatabaseHost = v; })
                .Add("o|port=", "database port, otherwise use default.", delegate (string v) { options.DatabasePort = Int32.Parse(v); })
                .Add("d|database=", "database name, default is 127.0.0.1.", delegate (string v) { options.DatabaseName = v; })
                .Add("u|user=", "database user, if not specified will use default user based on SeaChange or UpServer.", delegate (string v) { options.DatabaseUser = v; })
                .Add("p|pass=", "database password, if not specified will use default password based on SeaChange or UpServer.", delegate (string v) { options.DatabasePassword = v; })
                .Add("r|reboot", "auto reboot locked devices.  By default this is off and only an output list is logged down.", delegate (string v) { if (v != null) options.AutoReboot = true; })
                .Add("h|?|help", "show help", delegate (string v) { show_help = v != null; })
                .Add("m|smtpserver=", "SMTP server, if not provided email is disabled.", delegate (string v) { options.SmtpHost = v; })
                .Add("n|smtpport=", "SMTP port, default 25.", delegate (string v) { options.SmtpPort = Int32.Parse(v); })
                .Add("t|smtpto=", "Email recipient address, if not provided email is disabled.", delegate (string v) { options.SmtpTo = v; })
                .Add("f|smtpfrom=", "Email from address.", delegate (string v) { options.SmtpFrom = v; });

            List<string> extra;
            try
            {
                extra = p.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Write("bobbypin: ");
                Console.WriteLine(e.Message);
                Console.WriteLine("Try \"bobbypin --help\" for more information.");
                return;
            }

            if (show_help)
            {
                Usage(p);
                return;
            }

            if (extra.Count > 0)
            {
                options.Path = extra[0];
            }

            Console.WriteLine("Parsing logs from: {0}", options.Path);

            BasePicker picker = null;
            if (options.Mode == ServerMode.SeaChange)
            {
                picker = new SeaChangePicker();
                picker.Pick(options);
            }
            else if (options.Mode == ServerMode.UpServer)
            {
                picker = new UpServerPicker();
                picker.Pick(options);
            }

        }

        static void Head()
        {
            Console.WriteLine("");
            Console.WriteLine(DateTime.Now.ToString());
            Console.WriteLine("");
            Console.WriteLine("Aceso BobbyPin Version {0}", Assembly.GetExecutingAssembly().GetName().Version);
            Console.WriteLine("Copyright (C) 2016 Aceso. All rights reserved.");
            Console.WriteLine("");
        }

        static void Usage(OptionSet p)
        {
            Console.WriteLine("Usage: bobbypin [OPTIONS]+ <log file directory>");
            Console.WriteLine("Examples:");
            Console.WriteLine(@"bobbypin C:\TVBSTrace\wfes\logs\");
            Console.WriteLine(@"bobbypin -z -s=127.0.0.1 C:\inetpub\logs\logfiles\W3SVC5\");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }
    }
}
