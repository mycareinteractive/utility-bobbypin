﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace bobbypin
{
    public class UpServerPicker : BasePicker
    {
        public override void Pick(Options options)
        {
            base.Pick(options);

            var now = DateTime.Now;
            var utcNow = DateTime.UtcNow;

            if (utcNow.Hour == 0 && utcNow.Minute <= 10)
            {
                Console.WriteLine("IIS log file is just generated at midnight UTC and will not have enough data to check.  This opeartion is ignored.  Please retry after 12:10AM.");
                return;
            }

            var dateString = utcNow.ToString("yyMMdd");
            var fileNames = Directory.GetFiles(options.Path, string.Format("u_ex{0}.log", dateString));

            if (fileNames.Length < 1)
            {
                Console.WriteLine("Unable to find {0} in directory {1}", string.Format("u_ex{0}.log", dateString), options.Path);
                return;
            }

            // parse latest 2 log files
            var lockParser = new W3CLogParser();
            lockParser.Pattern = @"GET /api/v1/me";
            lockParser.ParseSpanInMinutes = 10;
            Console.WriteLine("\nParsing realtime log for current lockups...");

            // Parse the log
            lockParser.Parse(fileNames[0]);

            // These IPs has been doing get_user_data
            var userDataResult = lockParser.Result;
            string userDataIps = "''";
            foreach (var ip in userDataResult.Keys)
            {
                userDataIps += string.Format(",'{0}'", ip);
            }

            // check database for locked up devices (which do not show up in the hit address list)
            List<Device> lockedDevices = new List<Device>();

            string connString = string.Format("Server={0}; Database={1}; Uid={2}; Pwd={3};", options.DatabaseHost, options.DatabaseName, options.DatabaseUser, options.DatabasePassword);
            using (var connection = new MySqlConnection(connString))
            {
                connection.Open();

                string selectString = string.Format("SELECT d.Id, d.IPAddress, d.TestMode, CONCAT(l.Room, '-', l.Bed) AS RoomBed FROM devices AS d, deviceprovisions AS p, locations AS l WHERE d.Id = p.DeviceId AND p.LocationId = l.Id AND d.IPAddress NOT IN ({0}) ORDER BY RoomBed", userDataIps);
                using (var cmd = new MySqlCommand(selectString, connection))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                sbyte isTest = (sbyte)reader["TestMode"];
                                if (isTest == 0)
                                {
                                    var device = new Device("",
                                        reader["RoomBed"] as string,
                                        "",
                                        reader["Id"] as string,
                                        reader["IPAddress"] as string);

                                    if (!device.Room.StartsWith("999")
                                        && !string.IsNullOrEmpty(device.Ip)
                                        && device.Ip != "0.0.0.0"
                                        && device.Ip != "127.0.0.1")
                                    {
                                        lockedDevices.Add(device);
                                        Console.Write(device.ToString());
                                        Console.WriteLine(" - LOCKED!");
                                    }
                                }
                            }
                        }
                    }
                }
            }

            string eventMsg = string.Format("{0} LOCKED devices found", lockedDevices.Count);
            string eventDetails = "";
            Console.WriteLine(eventMsg);

            // log to file and try reboot if needed
            string proberPath = Path.Combine(Environment.CurrentDirectory, @"prober-local\prober.exe");
            int rebooted = 0;
            if (!File.Exists(proberPath))
            {
                options.AutoReboot = false; // can't reboot if Prober not found
            }
            if (options.AutoReboot)
            {
                int rebootAttempts = 1;
                Console.WriteLine("Attempting to auto reboot these devices...");

                dateString = DateTime.Now.ToString("yyyyMMdd");
                string filePath = Path.Combine(Environment.CurrentDirectory, string.Format("output.{0}.txt", dateString));
                using (StreamWriter outputFile = File.AppendText(filePath))
                using (var memStream = new MemoryStream())
                using (var outputBuff = new StreamWriter(memStream))
                {

                    outputFile.WriteLine(now.ToString());
                    outputFile.WriteLine(eventMsg);
                    outputFile.Flush();

                    outputBuff.WriteLine(now.ToString());
                    outputBuff.WriteLine(eventMsg);
                    outputBuff.Flush();

                    foreach (var device in lockedDevices)
                    {
                        outputFile.Write(device.ToString());
                        outputBuff.Write(device.ToString());
                        if (options.AutoReboot)
                        {
                            try
                            {
                                for (int i = 0; i < rebootAttempts; i++)
                                {
                                    var p = Process.Start(proberPath, string.Format("-reboot {0}", device.Ip));
                                    p.WaitForExit(15000);
                                    if (!p.HasExited)
                                    {
                                        try
                                        {
                                            p.Kill();
                                        }
                                        catch
                                        { }
                                    }

                                    if (p.ExitCode == 0)
                                    {
                                        rebooted++;
                                        outputFile.WriteLine(" - reboot ok");
                                        outputBuff.WriteLine(" - reboot ok");
                                        break;
                                    }

                                    if (i == rebootAttempts - 1)
                                    {
                                        outputFile.WriteLine(" - reboot failed");
                                        outputBuff.WriteLine(" - reboot failed");
                                    }
                                }
                            }
                            catch
                            {
                                outputFile.WriteLine(" - reboot failed");
                                outputBuff.WriteLine(" - reboot failed");
                            }
                        }
                        else
                        {
                            outputFile.WriteLine("");
                            outputBuff.WriteLine("");
                        }
                        outputFile.Flush();
                        outputBuff.Flush();
                    }

                    outputFile.WriteLine(Environment.NewLine);
                    outputBuff.WriteLine(Environment.NewLine);

                    outputBuff.Flush();
                    eventDetails = Encoding.Default.GetString((memStream.ToArray()));
                }
            }

            // log to event view
            if (options.AutoReboot)
            {
                var locked = lockedDevices.Count;
                eventMsg += string.Format(" {0} rebooted successfully", rebooted);
                WriteEventLog(eventMsg, eventDetails, locked == 0 ? EventLogEntryType.Information : EventLogEntryType.Warning);

                if (locked > 0)
                {
                    SendEmail("ALERT!! " + eventMsg, eventDetails);
                }
            }
        }
    }
}
