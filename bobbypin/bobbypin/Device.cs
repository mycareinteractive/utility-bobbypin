﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bobbypin
{
    public struct Device
    {
        public string Hotel;
        public string Room;
        public string Tv;
        public string Mac;
        public string Ip;

        public Device(string hotel, string room, string tv, string mac, string ip)
        {
            Hotel = hotel;
            Room = room;
            Tv = tv;
            Mac = mac;
            Ip = ip;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4}", Hotel, Room, Tv, Mac, Ip);
        }
    }
}
